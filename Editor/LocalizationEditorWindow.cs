using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
namespace PG.LocalizationManagementEditor
{
    using PG.LocalizationManagement;
    public class LocalizationEditorWindow : EditorWindow
    {
        private static TextAsset _localizationFile;
        private string _path;
        private static LocalizationData _localizationData;

        private Vector2 _languageScroll;
        private int _currentLanguageIndex;
        private List<string> _languages = new List<string>();

        private Vector2 _keyScroll;
        private int _currentKeyIndex;
        private List<string> _keys = new List<string>();
        private string _key;


        private void OnEnable()
        {
            if (_localizationData == null)
            {
                _localizationData = CreateInstance<LocalizationData>();
            }
            if (_localizationFile != null)
            {
                LoadLocalization();
            }
        }
        [MenuItem("Window/PG/Localization Window")]
        public static void OpenWindow()
        {
            var window = GetWindow<LocalizationEditorWindow>("Localization Window");
            _localizationData = CreateInstance<LocalizationData>();
            _localizationFile = null;
        }
        public static void OpenWindow(TextAsset textAsset)
        {
            var window = GetWindow<LocalizationEditorWindow>("Localization Window");
            window.titleContent.image = Resources.Load<Texture2D>("PG/LocalizationManagement/Icon");
            _localizationFile = textAsset;
        }
        void OnData()
        {
            if (_localizationData != null)
            {
                // �������� ������� ������ � ������
                if (_localizationData.languages.Count > 0 &&
                    _localizationData.languages[_currentLanguageIndex].items.Count > 0 &&
                    _keys.Count > 0)
                {
                    // �������� ������������ �������� ������� ������
                    if (_currentKeyIndex >= _localizationData.languages[_currentLanguageIndex].items.Count)
                    {
                        _currentKeyIndex = _localizationData.languages[_currentLanguageIndex].items.Count - 1;
                    }

                    // �������� ������������ �������� ������� ������
                    if (_currentLanguageIndex >= _localizationData.languages.Count)
                    {
                        _currentLanguageIndex = _localizationData.languages.Count - 1;
                    }

                    GUILayout.BeginVertical("box");

                    // ����� ����� � ����� � �������������� ��������
                    _currentLanguageIndex = EditorGUILayout.Popup("Language", _currentLanguageIndex, _languages.ToArray());
                    _currentKeyIndex = EditorGUILayout.Popup("Key", _currentKeyIndex, _keys.ToArray());

                    // �������� ������� ������ ����� ������ �������
                    if (_localizationData.languages[_currentLanguageIndex].items.Count > 0)
                    {
                        _localizationData.languages[_currentLanguageIndex].items[_currentKeyIndex].value = EditorGUILayout.TextArea(
                            _localizationData.languages[_currentLanguageIndex].items[_currentKeyIndex].value,
                            new GUIStyle(EditorStyles.textArea) { wordWrap = true }, GUILayout.MaxWidth(1000)
                        );
                    }

                    GUILayout.EndVertical();
                }
            }
        }

        private void OnHeader()
        {
            GUILayout.BeginHorizontal();
            _localizationFile = (TextAsset)EditorGUILayout.ObjectField(_localizationFile, typeof(TextAsset),false);

            if (_localizationFile != null)
            {
                if (GUILayout.Button("Load file", GUILayout.Width(140)))
                {
                    LoadLocalization();
                    GUI.FocusControl(null);
                }
                if (GUILayout.Button("Save file", GUILayout.Width(140)))
                {
                    SaveLocalization();
                    GUI.FocusControl(null);
                }
            }
            if (GUILayout.Button("Save as", GUILayout.Width(140)))
            {
                string newPath = EditorUtility.SaveFilePanel("Save as", Application.dataPath, "Localization", "txt");

                if (!string.IsNullOrEmpty(newPath))
                {
                    // ���������� �����
                    File.WriteAllText(newPath, JsonUtility.ToJson(_localizationData, true));
                    AssetDatabase.Refresh();

                    // �������������� ������� ���� � ������������� ���� ��� �������� � Unity
                    string relativePath = "Assets" + newPath.Substring(Application.dataPath.Length);
                    _localizationFile = AssetDatabase.LoadAssetAtPath<TextAsset>(relativePath);
                }
                SaveLocalization();
                GUI.FocusControl(null);
            }
            GUILayout.EndHorizontal();
        }
        void OnKeysPanel()
        {
            if (_localizationData != null && _localizationData.languages.Count > 0)
            {
                GUILayout.BeginVertical("box", GUILayout.MaxWidth(300), GUILayout.MinWidth(250));
                GUILayout.Label("Keys", new GUIStyle(EditorStyles.boldLabel) { fontSize = 20, alignment = TextAnchor.MiddleCenter });
                GUILayout.BeginHorizontal("box");
                if (GUILayout.Button("Update"))
                {
                    LoadKeysList();
                    GUI.FocusControl(null);
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal("box");
                GUILayout.Label("Key");
                _key = EditorGUILayout.TextArea(_key, new GUIStyle(EditorStyles.textArea) { wordWrap = true });
                if (GUILayout.Button("Add"))
                {
                    AddKey(_key);
                    GUI.FocusControl(null);
                    _key = "";
                }
                GUILayout.EndHorizontal();
                _keyScroll = GUILayout.BeginScrollView(_keyScroll, "box", GUILayout.MaxWidth(300), GUILayout.MinWidth(250));
                if (_localizationData.languages.Count > 0)
                {
                    for (int i = 0; i < _localizationData.languages[0].items.Count; i++)
                    {
                        GUILayout.BeginHorizontal();
                        _localizationData.languages[0].items[i].key = EditorGUILayout.TextArea(_localizationData.languages[0].items[i].key, new GUIStyle(EditorStyles.textArea) { wordWrap = true });
                        if (GUILayout.Button("->", GUILayout.Width(30)))
                        {
                            _currentKeyIndex = i;
                            GUI.FocusControl(null);
                        }
                        if (GUILayout.Button("Update", GUILayout.Width(60)))
                        {
                            UpdateKeys(_localizationData.languages[0].items[i].key, i);
                            LoadKeysList();
                        }
                        if (GUILayout.Button("X", GUILayout.Width(30)))
                        {
                            RemoveKey(_keys[i]);
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                GUILayout.EndScrollView();
                GUILayout.EndVertical();
            }
        }
        void OnLanguagesPanel()
        {
            if (_localizationData != null)
            {
                _languageScroll = GUILayout.BeginScrollView(_languageScroll, "box", GUILayout.MaxWidth(200), GUILayout.MinWidth(150));
                GUILayout.Label("Languages", new GUIStyle(EditorStyles.boldLabel) { fontSize = 20, alignment = TextAnchor.MiddleCenter });
                if (GUILayout.Button("Update"))
                {
                    LoadLanguagesList();
                    GUI.FocusControl(null);
                }
                if (GUILayout.Button("Add"))
                {
                    AddLanguage();
                    GUI.FocusControl(null);
                }
                if (_localizationData.languages.Count > 0)
                {
                    for (int i = 0; i < _localizationData.languages.Count; i++)
                    {
                        GUILayout.BeginHorizontal();
                        _localizationData.languages[i].languageCode = EditorGUILayout.TextField(_localizationData.languages[i].languageCode);
                        if (GUILayout.Button("X"))
                        {
                            RemoveLanguage(i);
                            GUI.FocusControl(null);
                        }
                        GUILayout.EndHorizontal();
                    }
                }
                GUILayout.EndScrollView();
            }
        }
        private void OnGUI()
        {
            OnHeader();
            GUILayout.BeginHorizontal();
            OnLanguagesPanel();
            OnKeysPanel();
            OnData();
            GUILayout.EndHorizontal();

            // ���������, ��� ������ �������� ��� dirty
            if (GUI.changed)
            {
                Undo.RecordObject(_localizationData, "Localization change.");
                EditorUtility.SetDirty(_localizationData);
            }
        }

        void AddKey(string value)
        {
            if (!ContainsKey(value))
            {
                Undo.RecordObject(_localizationData, "Add Key");
                for (int i = 0; i < _localizationData.languages.Count; i++)
                {
                    _localizationData.languages[i].items.Add(new LocalizationItem() { key = value });
                }
                LoadKeysList();
            }
        }

        void RemoveKey(string value)
        {
            if (ContainsKey(value))
            {
                Undo.RecordObject(_localizationData, "Remove Key");
                for (int i = 0; i < _localizationData.languages.Count; i++)
                {
                    for (int a = 0; a < _localizationData.languages[i].items.Count; a++)
                    {
                        if (_localizationData.languages[i].items[a].key == value)
                        {
                            _localizationData.languages[i].items.RemoveAt(a);
                            break;
                        }
                    }
                }
                LoadKeysList();
                _currentKeyIndex = 0;
            }
        }

        void AddLanguage()
        {
            Undo.RecordObject(_localizationData, "Add Language");
            if (_localizationData.languages.Count > 0)
            {
                _localizationData.languages.Add(new LanguageData(_localizationData.languages[0]));
                _languages.Add(_localizationData.languages[0].languageCode);
            }
            else
            {
                _localizationData.languages.Add(new LanguageData());
                _languages.Add("");
            }
        }

        void RemoveLanguage(int value)
        {
            Undo.RecordObject(_localizationData, "Remove Language");
            _currentLanguageIndex = 0;
            _localizationData.languages.RemoveAt(value);
            _languages.RemoveAt(value);
        }

        void UpdateKeys(string key, int targetIndex)
        {
            Undo.RecordObject(_localizationData, "Update Key");
            for (int i = 0; i < _localizationData.languages.Count; i++)
            {
                _localizationData.languages[i].items[targetIndex].key = key;
            }
        }

        LocalizationItem GetKey(string key, int index)
        {
            foreach (var item in _localizationData.languages[index].items)
            {
                if (item.key == key)
                {
                    return item;
                }
            }
            return null;
        }
        bool ContainsKey(string key)
        {
            foreach (var language in _localizationData.languages)
            {
                foreach (var item in language.items)
                {
                    if (item.key == key)
                    {
                        return true;
                    }
                }
            }
            return false;
        }



        private void LoadLanguagesList()
        {
            _languages.Clear();
            for (int i = 0; i < _localizationData.languages.Count; i++)
            {
                _languages.Add(_localizationData.languages[i].languageCode);
            }
        }
        private void LoadKeysList()
        {
            _keys.Clear();
            for (int i = 0; i < _localizationData.languages[0].items.Count; i++)
            {
                _keys.Add(_localizationData.languages[0].items[i].key);
            }
            if (_localizationData.languages[0].items.Count > 0)
            {
                _currentKeyIndex = _localizationData.languages[0].items.Count - 1;
            }
        }

        public void LoadLocalization()
        {
            JsonUtility.FromJsonOverwrite(_localizationFile.text, _localizationData);
            LoadLanguagesList();
            LoadKeysList();
        }
        public void SaveLocalization()
        {
            _path = AssetDatabase.GetAssetPath(_localizationFile);
            File.WriteAllText(_path, JsonUtility.ToJson(_localizationData, true));
            AssetDatabase.Refresh();
        }
        public void SetLocalizationPath()
        {
            _path = EditorUtility.OpenFilePanel("Select file", Application.dataPath, "");
        }
    }
}
