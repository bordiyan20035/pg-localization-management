using UnityEditor;
using UnityEngine;
namespace PG.LocalizationManagementEditor
{
    using PG.LocalizationManagement;
    [CustomEditor(typeof(LocalizationManager))]
    public class LocalizationManagerEditor : Editor
    {
        private LocalizationManager _localizationManager;
        private void OnEnable()
        {
            _localizationManager = (LocalizationManager)target;
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Load file"))
            {
                _localizationManager.LoadLocalization();
            }
            if (GUILayout.Button("Open window"))
            {
                LocalizationEditorWindow.OpenWindow(_localizationManager.localizationFile);
            }
        }
    }

}