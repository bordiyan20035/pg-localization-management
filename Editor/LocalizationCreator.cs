using UnityEditor;
using UnityEngine;

namespace PG.LocalizationManagementEditor
{
    public static class LocalizationCreator
    {
        [MenuItem("GameObject/PG/Localization Manager", false, 0)]
        static void AddPrefab()
        {
            GameObject prefab = Resources.Load("PG/LocalizationManagement/LocalizationManager") as GameObject;
            if (Selection.gameObjects.Length > 0)
            {

                foreach (var item in Selection.gameObjects)
                {
                    GameObject createdPrefab = GameObject.Instantiate(prefab, item.transform);
                    createdPrefab.name = "Localization Manager";
                    Selection.activeObject = createdPrefab;
                }
            }
            else
            {
                GameObject createdPrefab = GameObject.Instantiate(prefab);
                createdPrefab.name = "Localization Manager";
                Selection.activeObject = createdPrefab;
            }
        }

    }
}
