﻿using System.Collections.Generic;
using UnityEngine;
namespace PG.LocalizationManagement
{
    [System.Serializable]
    public class LocalizationData : ScriptableObject
    {
        public List<LanguageData> languages = new List<LanguageData>();
    }
}
