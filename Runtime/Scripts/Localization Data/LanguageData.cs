﻿using System.Collections.Generic;
namespace PG.LocalizationManagement
{
    [System.Serializable]
    public class LanguageData
    {
        public string languageCode;
        public List<LocalizationItem> items = new List<LocalizationItem>();

        public LanguageData(LanguageData language) {
            languageCode = language.languageCode;
            items = new List<LocalizationItem>(language.items);
        }
        public LanguageData()
        {
            
        }
    }
}
