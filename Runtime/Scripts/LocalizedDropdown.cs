﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;
namespace PG.LocalizationManagement
{
    public class LocalizedDropdown : MonoBehaviour
    {
        [field: SerializeField] public string[] keys { get; private set; }
        [SerializeField] private TMP_Dropdown _dropdown;

        [ContextMenu("Get Cache")]
        void GetCache()
        {
            _dropdown = GetComponent<TMP_Dropdown>();
        }
        void Start()
        {
            UpdateDropdown(LocalizationManager.Instance.currentLanguage);
            LocalizationManager.Instance.onLanguageChanged += UpdateDropdown;
        }

        void OnDestroy()
        {
            LocalizationManager.Instance.onLanguageChanged -= UpdateDropdown;
        }
        void UpdateDropdown(string language = "")
        {
            _dropdown.ClearOptions();
            List<string> values = new List<string>();
            foreach (string key in keys)
            {
                values.Add(LocalizationManager.Instance.GetLocalizedValue(key));
            }
            _dropdown.AddOptions(values);
        }
    }

}
