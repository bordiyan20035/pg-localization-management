﻿using UnityEngine;
using TMPro;
namespace PG.LocalizationManagement
{
    public class LocalizedText : MonoBehaviour
    {
        [SerializeField] private TMP_Text _langugageText;
        public string key;

        [ContextMenu("Get Cache")]
        void GetCache()
        {
            _langugageText = GetComponent<TMP_Text>();
        }

        void Start()
        {
            UpdateText();
            LocalizationManager.Instance.onLanguageChanged += UpdateText;
        }

        void OnDestroy()
        {
            LocalizationManager.Instance.onLanguageChanged -= UpdateText;
        }

        void UpdateText(string language = "")
        {
            if (_langugageText != null)
            {
                _langugageText.text = LocalizationManager.Instance.GetLocalizedValue(key);
            }
        }
    }

}
