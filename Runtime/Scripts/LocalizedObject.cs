﻿using UnityEngine;
using TMPro;
namespace PG.LocalizationManagement
{
    public class LocalizedObject : MonoBehaviour
    {
        [SerializeField] private LanguageElementObject[] _languageElementObjects;

        [System.Serializable]
        public class LanguageElementObject
        {
            [field:SerializeField] public string language { get; private set; }
            [field: SerializeField] public GameObject gameObject { get; private set; }
        }

        void Start()
        {
            UpdateGameObjects(LocalizationManager.Instance.currentLanguage);
            LocalizationManager.Instance.onLanguageChanged += UpdateGameObjects;
        }

        void OnDestroy()
        {
            LocalizationManager.Instance.onLanguageChanged -= UpdateGameObjects;
        }

        void UpdateGameObjects(string language)
        {
            foreach (var item in _languageElementObjects)
            {
                item.gameObject.SetActive(item.language == language);
            }
        }
    }

}
