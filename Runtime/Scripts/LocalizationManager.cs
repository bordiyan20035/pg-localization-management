using UnityEngine;
namespace PG.LocalizationManagement
{
    public class LocalizationManager : MonoBehaviour
    {
        public static LocalizationManager Instance;
        [SerializeField] private string _saveName = "Localization";
        [field:SerializeField] public TextAsset localizationFile {  get; private set; }
        private LocalizationData _localizationData;
        [field:SerializeField] public TextAsset[] subLocalizationFiles {  get; private set; }
        private LocalizationData[] _subLocalizationDatas;
        public LocalizationData localizationData => _localizationData;

        [SerializeField] private string _currentLanguage = "en";
        public string currentLanguage => _currentLanguage;
        public event System.Action<string> onLanguageChanged;

        void Awake()
        {
            _localizationData = ScriptableObject.CreateInstance<LocalizationData>();
            _subLocalizationDatas = new LocalizationData[subLocalizationFiles.Length];

            for (int i = 0; i < _subLocalizationDatas.Length; i++)
            {
                _subLocalizationDatas[i] = ScriptableObject.CreateInstance<LocalizationData>();
            }

            if (Instance == null)
            {
                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
                Instance = this;
                LoadLocalization();
            }
            else
            {
                Destroy(gameObject);
            }
            Load();
        }
        [ContextMenu("Load")]
        public void LoadLocalization()
        {
            JsonUtility.FromJsonOverwrite(localizationFile.text, _localizationData);
            if (subLocalizationFiles.Length > 0)
            {
                for (int i = 0; i < subLocalizationFiles.Length; i++)
                {
                    JsonUtility.FromJsonOverwrite(subLocalizationFiles[i].text, _subLocalizationDatas[i]);
                }
            }
        }
        public string GetLocalizedValue(string key)
        {

            foreach (var itemKey in _localizationData.languages)
            {
                if (itemKey.languageCode == _currentLanguage)
                {
                    foreach (var itemValue in itemKey.items)
                    {
                        if (itemValue.key == key)
                        {
                            return itemValue.value;
                        }
                    }
                }
            }
            for (int i = 0; i < _subLocalizationDatas.Length; i++)
            {
                foreach (var itemKey in _subLocalizationDatas[i].languages)
                {
                    if (itemKey.languageCode == _currentLanguage)
                    {
                        foreach (var itemValue in itemKey.items)
                        {
                            if (itemValue.key == key)
                            {
                                return itemValue.value;
                            }
                        }
                    }
                }
            }
            return key;
        }

        public void SetLanguage(string languageCode)
        {
            foreach (var itemKey in _localizationData.languages)
            {
                if (itemKey.languageCode == _currentLanguage)
                {
                    _currentLanguage = languageCode;
                    onLanguageChanged?.Invoke(_currentLanguage);
                }
            }
        }
        public void SetLanguage(int languageCode)
        {
            _currentLanguage = _localizationData.languages[languageCode].languageCode;
            Save();
            onLanguageChanged?.Invoke(_currentLanguage);
        }
        void Load()
        {
            if (PlayerPrefs.HasKey(_saveName))
            {
                _currentLanguage = PlayerPrefs.GetString(_saveName);
            }
        }
        void Save()
        {
            PlayerPrefs.SetString(_saveName, _currentLanguage);
        }
    }

}
