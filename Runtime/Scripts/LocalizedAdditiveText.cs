using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace PG.LocalizationManagement
{
    public class LocalizedAdditiveText : MonoBehaviour
    {
        [SerializeField] private TMP_Text _descriptionText;
        [SerializeField] private LanguageElementObject[] _languageElementObjects;
        [System.Serializable]
        public class LanguageElementObject
        {
            [field: SerializeField] public string language { get; private set; }
            [field: SerializeField][field:TextArea(3,10)] public string text { get; private set; }
        }
        private void Awake()
        {
            Init();
        }
        async void Init()
        {
            await Task.Delay(20);
            LocalizationManager.Instance.onLanguageChanged += DisplayText;
        }
        private void OnDestroy()
        {
            LocalizationManager.Instance.onLanguageChanged -= DisplayText;
        }
        // Start is called before the first frame update
        void Start()
        {
            DisplayText(LocalizationManager.Instance.currentLanguage);
        }
        [ContextMenu("Get Cache")]
        void GetCache()
        {
            _descriptionText = GetComponent<TMP_Text>();
        }

        async void DisplayText(string language)
        {
            await Task.Delay(30);
            for (int i = 0; i < _languageElementObjects.Length; i++)
            {
                if (_languageElementObjects[i].language == language)
                {
                    _descriptionText.text = _languageElementObjects[i].text;
                    break;
                }
            }
        }
    }
}
